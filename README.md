
# Laravel - Vue - Chat Application

A Simple messaging app with vueJS and Laravel.

## Requirements

PHP > 7.4

NodeJS > 14.17

## Installation

Clone the repository
```bash
$ git clone https://gitlab.com/Jayesh-Paunikar/chat-app.git
```
Once installed, Switch to the repo folder

```bash
cd chat-app
```
Install all the dependencies using composer
```bash
composer install
```
Copy the example env file and make the required configuration changes in the .env file
```bash
cp .env.example .env
```
Generate a new application key
```bash
php artisan key:generate
```

Run the database migrations (**Set the database connection in .env before migrating**)
```bash
php artisan migrate
```
Generate a new passport authentication secret key
```bash
php artisan passport:install
```

Install all vueJS dependencies
```bash
npm install
```
run these commands with diferent terminal parallelly
```bash
npm run dev

php artisan serve
```

