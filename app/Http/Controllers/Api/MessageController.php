<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendMessage;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function send_message(SendMessage $request){

        $request = $request->validated();
        $request['receiver_id'] = $request['user_id'];
        $request['sender_id'] = auth()->user()->id;
        Message::create($request);
        $messages_data = $this->get_message($request['user_id'],1);
        return response()->json(['message'=> 'message sent!','messages'=> $messages_data], 200);
    }

    public function get_message($user_id,$from=0){
        $data = Message::where('receiver_id',$user_id)->orWhere('sender_id',$user_id)->get();
        if($from == 1){
            return $data;
        }
        return response()->json(['message'=> 'message retrived!','messages'=> $data], 200);

    }

    public function notification(){
        $data = Message::with('sender')->where('receiver_id',auth()->user()->id)->orderBy('id','DESC')->get();
        foreach($data as $d){
            if($d->is_seen == 0){
                Message::where('id',$d->id)->update(['is_seen' => 1, 'seen_at'=> Carbon::now()]);
            }
        }
        return response()->json(['message'=> 'notification retrived!','messages'=> $data], 200);
    }


}
