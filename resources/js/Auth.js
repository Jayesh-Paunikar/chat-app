import axios from 'axios';
import store from './store/index.js';

class Auth {
    constructor () {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.token;
    }

    check () {
        return store.state.token;
    }
    logout () {
        window.localStorage.clear();

    }
}
export default new Auth();
