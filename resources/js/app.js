/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Auth from './Auth.js';
import store from './store/index.js';
import JQuery from "jquery";

Vue.prototype.auth = Auth;
Vue.use(VueAxios, axios);
axios.defaults.baseURL = 'http://127.0.0.1:8000/api/';
axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.token;

import App from './app.vue';
import router from './routes';

const app = new Vue({
    el: '#app',
    router,
    store,
    JQuery,
    Auth,
    render: h => h(App),
});



