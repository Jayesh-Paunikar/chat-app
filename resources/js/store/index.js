import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        user: null,
        token: null,
        is_autheticated: false,

    },
    plugins: [createPersistedState()],
    mutations: {
        login(state,data){
            state.user = data.user;
            state.token = data.access_token;
            state.is_autheticated = true;
        },
        logout(state){
            state.user = null;
            state.token = null;
            state.is_autheticated = false;
            window.localStorage.clear();
        }

    },
    actions:{

    },
    getters: {

    }


})
export default store;
